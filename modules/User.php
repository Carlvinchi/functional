<?php
    /* @params $user_name, $user_email, $password, $name */
    $create_user = function($user_email,$phone_no,$first_name,$last_name,
    $digital_address,$address_street,$address_city,$address_region,$password,$connect, $userExists,$create_date)
    {
        
        if($userExists($user_email,$connect) == 'Yes')
        {
            return "User already registered";
            exit;
        }
        
        $customer_id = rand(1,50).time().''.rand(51,99);
        $reg_date   = $create_date();
        
        $hashed_password = password_hash($password,PASSWORD_BCRYPT, ["cost"=>15]);
        
        $pre_stmt = $connect->prepare("INSERT INTO users (
        `customer_id`,`user_email`,`phone_no`,
         `first_name`,`last_name`,`digital_address`,
         `address_street`, `address_city`, `address_region`,
         `password`, `reg_date`
         )
        VALUES (?,?,?,?,?,?,?,?,?,?,?)
        ");
        $pre_stmt->bind_param("sssssssssss",$customer_id,$user_email, $phone_no,$first_name,$last_name,
        $digital_address,$address_street,$address_city,$address_region,$hashed_password, $reg_date);
        
        $res = $pre_stmt->execute() or die($connect->error);
        if($res)
            return "Success";
        
        else
            return "Error";
            

    };


     // Check if user already registered
     $userExists = function($email,$connect)
     {
         $pre_stmt = $connect->prepare("SELECT user_id FROM users WHERE user_email = ? ");
         $pre_stmt->bind_param("s", $email);
         $pre_stmt->execute() or die($connect->error);
         $result = $pre_stmt->get_result();
         if($result->num_rows > 0)
             return 'Yes';
         else
             return 'No';
         
     };

     $user_login = function($user_email, $password,$create_time,$connect)
            {
                $pre_stmt = $connect->prepare("SELECT * FROM users WHERE user_email = ? ");
                $pre_stmt->bind_param("s", $user_email) ;
                $pre_stmt->execute() or die($connect->error);
                $result = $pre_stmt->get_result();
                
                if($result->num_rows < 1)
                {
                    return "Not Registered, Please Register!";
                    exit;
                }

                $row = $result->fetch_assoc();
                if(password_verify($password, $row["password"]))
                {   
                    session_start();
                
                    $last_login = $create_time();

                    $_SESSION['user_email'] = $row['user_email'];
                    $_SESSION['customer_id'] = $row['customer_id'];
                    $_SESSION['phone_no'] = $row['phone_no'];
                    $_SESSION['first_name'] = $row['first_name'];
                    $_SESSION['last_name'] = $row['last_name'];
                    $_SESSION['user_id'] = $row["user_id"];

                    $pre_stmt = $connect->prepare("UPDATE users SET last_login = ? WHERE user_email = ?");
                    $pre_stmt->bind_param("ss",$last_login, $user_email);
                    $pre_stmt->execute() or die($connect->error);

                    return "Success";
                }
                else
                    return "invalid password";
                
            };

    
            $find_user = function($user_email="", $user_id="", $connect,$get_data)
            {
                $pre_stmt = $connect->prepare("SELECT * FROM users WHERE user_email = ? OR user_id = ?");
                $pre_stmt->bind_param("ss", $user_email,$user_id) ;
                $result = $get_data($pre_stmt,$connect);
                
                if(empty($result))
                    return "User does not exist";
                else
                    return $result;
                 
            };

          $find = function ($connect,$get_data)
            {
                $pre_stmt = $connect->prepare("SELECT * FROM users");
                
                $result = $get_data($pre_stmt,$connect);
                
                if(empty($result))
                    return "User does not exist";
                else
                    return $result;
                 
            };


            $change_password = function ($old_password,$new_password,$user_id,$connect,$find_user)
            {   
                $check_password = $find_user("",$user_id,$connect)[0]["password"];
    
                if(password_verify($old_password, $check_password) !== TRUE)
                {
                    return "invalid old password";
                    exit;
                }

                return function() use($new_password,$user_id,$connect) {
                    $hashed_password = password_hash($new_password,PASSWORD_BCRYPT, ["cost"=>15]);
                    $pre_stmt = $connect->prepare("UPDATE users SET  
                     `password` = ? WHERE user_id = ? ");
                    $pre_stmt->bind_param("ss",$hashed_password, $user_id);
                    $result = $pre_stmt->execute() or die($connect->error);
                    if($result)
                        return "Success";
                        
                    else
                        return "Error";
                };
            
            };
        
            $edit_profile_no_password = function ($user_email,$phone_no,$first_name,$last_name,
            $digital_address,$address_street,$address_city,$address_region,$user_id,$connect){
        
                $pre_stmt = $connect->prepare("UPDATE users SET  
                `user_email` = ?,`phone_no` = ?,
                 `first_name` = ?,`last_name` = ?,`digital_address` = ?,
                 `address_street` = ?, `address_city` = ?, `address_region` = ?
                 WHERE user_id = ?
                ");
                $pre_stmt->bind_param("sssssssss", $user_email,$phone_no,$first_name,$last_name,
                $digital_address,$address_street,$address_city,$address_region,$user_id);
                $result = $pre_stmt->execute() or die($connect->error);
                if($result)
                    return "Success";

                else
                    return "Error"; 
                
            };

        


            $get_data = function ($pre_stmt,$connect) 
            {
                $pre_stmt->execute() or die($connect->error); 
                $result = $pre_stmt->get_result();
                if(!$result)
                {
                    return $connect->error;
                    } 
        
                $data= array();
                
                 while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) 
                 {
                        $data[]=$row;            
                 }
                    return $data;
            };
