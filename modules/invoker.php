<?php
class User{
            // creating a class constructor
            public function __construct($connect)
            {
                $this->conn = $connect;
                // Attempt create table
                $sql = "CREATE TABLE IF NOT EXISTS users (
                    user_id BIGINT(20) AUTO_INCREMENT,
                    customer_id BIGINT(20) UNIQUE,
                    user_email VARCHAR(64),
                    phone_no CHAR(10),
                    first_name CHAR(20),
                    last_name CHAR(20),
                    digital_address CHAR(32),
                    address_street CHAR(120),
                    address_city CHAR(60),
                    address_region CHAR(60),
                    password VARCHAR(255),
                    reg_date DATETIME,
                    last_login DATETIME,
                    PRIMARY KEY(user_id) 
                    )";


                if($this->conn->query($sql))
                {

                    return "Success";
                } 
                else
                {
                    return "Error";
                }
            }
}