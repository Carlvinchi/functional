<?php
date_default_timezone_set("GMT");

$create_time = function(){
    $time  = new DateTime(); //this returns the current date time
    return $time->format('Y-m-d H:i:s');
};

  $create_date = function(){
    $date   = date('Y-m-d'); //this returns the current date time
    return $date;
};