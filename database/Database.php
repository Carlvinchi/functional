<?php
    
        /*Establish database connection
        @params
        server_name, $user_name, $password, db_name 
        http://localhost/todo/backend/database/db.php
        */
        /* creating anonymous function and assigning it to a variable*/
        $con = function($server_name,$user_name,$password,$db_name)
        {
            $link = new mysqli($server_name,$user_name,$password,$db_name);
            // Check connection
            if ($link->connect_error) {
                return ("Connection failed: " . $link->connect_error);
            }
            return $link;
        
        };

        $con1 = function($server_name,$user_name,$password)
        {
            $link = mysqli_connect($server_name,$user_name,$password);
            // Check connection
            if ($link->connect_error) {
                return ("Connection failed: " . $link->connect_error);
            }
            return $link;
        
        };

        
        /* creating anonymous function and assigning it to a variable*/
       $create_db =  function($server_name,$user_name,$password,$db_name,$con)
        {
            $sql = "CREATE DATABASE IF NOT EXISTS ".$db_name;
            $result =$con($server_name,$user_name,$password);

            if($result->query($sql) === TRUE){
                return "Database Created";
            } 
            else{
                return "ERROR: Could not able to execute $sql. " . $result->error;
            }
            
        };

    

    //$db = new Database();
    //$result = $db->create_db("localhost","root","","mav");
