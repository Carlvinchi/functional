#non tail recursion
def n_recurse(number):
    if number <=1: return 1
    return number*n_recurse(number-1)

# tail recursion
def tail_recurse(number,acc):
    if number <= 1: return acc
    return tail_recurse(number-1,number*acc)

print(tail_recurse(10000,1))
#print(n_recurse(10000))